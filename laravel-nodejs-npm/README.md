# What's this?
This is a NodeJS npm [Docker](http://www.docker.com) image. This is intended as an image to support development of your laravel application in as part of the [LaravelKit](https://gitlab.com/czechbox).


## Credit where it's due
Unlike other images in LaravelKit, this one was added by me to deal with cross-platform development issues on our team.
