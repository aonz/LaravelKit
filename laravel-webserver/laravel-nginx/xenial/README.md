
# What's this?
This is an Ubuntu 16.04 based nginx [Docker](http://www.docker.com) image.

This is intended as an image to serve your laravel application from with the [LaravelKit](https://gitlab.com/czechbox).

## Credit where it's due
It's based on the awesome work of [Dylan Lindgren](http://dylanlindgren.com/docker-for-the-laravel-framework), [Kyle Ferguson](https://kyleferg.com/laravel-development-with-docker/) and [PHPDocker.io](http://phpdocker.io). I've built it all on the official [Ubuntu 14.04 - Trusty Tahr](https://hub.docker.com/_/ubuntu/) base image (because that's my preference) and re-jigged a quite a few things to update them for 2017. Yes an Xenial version is coming.
