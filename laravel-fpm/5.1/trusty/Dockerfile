#start with ubuntu
FROM aonz/laravel-php-base:trusty
MAINTAINER Jef Vratny - https://gitlab.com/czechbox
ENV DEBIAN_FRONTEND noninteractive
ENV FPM_USER www-data
ENV FPM_GROUP www-data

#install common packages for php-fpm
RUN apt-get update && apt-get install --no-install-recommends -y \
  php5-fpm \
  php5-gd \
  php5-geoip \
  php5-imagick \
  php5-memcache \
  php5-memcached \
  php5-mongo \
  php5-mysqlnd \
  php5-pgsql \
  php5-redis \
  php5-sqlite \
  php5-xdebug \
  php5-xmlrpc \
  php5-xcache \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

#update configs as needed
RUN sed -i "s/;date.timezone =.*/date.timezone = UTC/" /etc/php5/fpm/php.ini && \
  sed -i "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/" /etc/php5/fpm/php.ini && \
  sed -i "s/display_errors = Off/display_errors = stderr/" /etc/php5/fpm/php.ini && \
  sed -i "s/upload_max_filesize = 2M/upload_max_filesize = 30M/" /etc/php5/fpm/php.ini && \
  sed -i "s/;opcache.enable=0/opcache.enable=0/" /etc/php5/fpm/php.ini && \
  sed -i -e "s/;daemonize\s*=\s*yes/daemonize = no/g" /etc/php5/fpm/php-fpm.conf && \
  sed -i '/^listen = /clisten = 9000' /etc/php5/fpm/pool.d/www.conf && \
  sed -i '/^listen.allowed_clients/c;listen.allowed_clients =' /etc/php5/fpm/pool.d/www.conf && \
  sed -i '/^;catch_workers_output/ccatch_workers_output = yes' /etc/php5/fpm/pool.d/www.conf && \
#plug in the environmental variable from docker compose
  sed -i '/^;env\[TEMP\] = .*/aenv[DB_PORT_3306_TCP_ADDR] = $DB_PORT_3306_TCP_ADDR' /etc/php5/fpm/pool.d/www.conf && \
  echo "xdebug.max_nesting_level = 300" >> /etc/php5/mods-available/xdebug.ini

COPY fpm-entrypoint /usr/local/bin/

ENTRYPOINT ["/usr/local/bin/fpm-entrypoint"]

RUN chmod u=rwx /usr/local/bin/fpm-entrypoint && mkdir -p /workspace

VOLUME ["/workspace"]

WORKDIR /workspace

EXPOSE 9000

ENV DEBIAN_FRONTEND teletype

CMD ["php5-fpm"]
