#start with ubuntu
FROM aonz/laravel-php-base:xenial
MAINTAINER Jef Vratny - https://gitlab.com/czechbox
ENV DEBIAN_FRONTEND noninteractive
#install common packages for php-fpm
RUN apt-get update && apt-get install --no-install-recommends -y \
  php-fpm \
  php-gd \
  php-geoip \
  php-imagick \
  php-memcache \
  php-memcached \
  php-mongodb \
  php-mysqlnd \
  php-opcache \
  php-pgsql \
  php-redis \
  php-sqlite3 \
  php-xdebug \
  php-xmlrpc \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

#update configs as needed
RUN sed -i "s/;date.timezone =.*/date.timezone = UTC/" /etc/php/7.1/fpm/php.ini && \
  sed -i "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/" /etc/php/7.1/fpm/php.ini && \
  sed -i "s/display_errors = Off/display_errors = stderr/" /etc/php/7.1/fpm/php.ini && \
  sed -i "s/upload_max_filesize = 2M/upload_max_filesize = 30M/" /etc/php/7.1/fpm/php.ini && \
  sed -i "s/;opcache.enable=0/opcache.enable=0/" /etc/php/7.1/fpm/php.ini && \
  sed -i -e "s/;daemonize\s*=\s*yes/daemonize = no/g" /etc/php/7.1/fpm/php-fpm.conf && \
  sed -i '/^listen = /clisten = 9000' /etc/php/7.1/fpm/pool.d/www.conf && \
  sed -i '/^listen.allowed_clients/c;listen.allowed_clients =' /etc/php/7.1/fpm/pool.d/www.conf && \
  sed -i '/^;catch_workers_output/ccatch_workers_output = yes' /etc/php/7.1/fpm/pool.d/www.conf && \
#plug in the environmental variable from docker compose
  sed -i '/^;env\[TEMP\] = .*/aenv[DB_PORT_3306_TCP_ADDR] = $DB_PORT_3306_TCP_ADDR' /etc/php/7.1/fpm/pool.d/www.conf && \
  echo "xdebug.max_nesting_level = 300" >> /etc/php/7.1/mods-available/xdebug.ini

COPY fpm-entrypoint /usr/local/bin/

ENTRYPOINT ["/usr/local/bin/fpm-entrypoint"]

RUN chmod u=rwx /usr/local/bin/fpm-entrypoint && mkdir -p /workspace && mkdir /run/php

VOLUME ["/workspace"]

WORKDIR /workspace

EXPOSE 9000

ENV DEBIAN_FRONTEND teletype

CMD ["/usr/sbin/php-fpm7.0"]
